package Pages;

import Helpers.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class BaseSingUpPage extends BasePage
{
    protected By EmailEntry = By.id("email");

    public BaseSingUpPage(WebDriver driver) {
        super(driver);
    }

    @Step
    public BaseSingUpPage enterEmail(String text)
    {
        Enter(EmailEntry, text);
        TakeScreenshot(driver,"enterEmail");

        return this;
    }
}
