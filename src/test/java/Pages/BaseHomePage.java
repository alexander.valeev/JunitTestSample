package Pages;

import Helpers.AppManager;
import Helpers.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public abstract class BaseHomePage extends BasePage
{
    protected By carousel = By.id("carousel");
    protected By getStarted = By.xpath("//a[contains(@href,'/signup') and contains(.,\"Get Started – It’s Free!\")]");

    public BaseHomePage(WebDriver driver) {
        super(driver);
    }

    public BaseHomePage tapCarousel()
    {
        String oldCarouselStep = Wait(carousel).getAttribute("data-step");
        Tap(carousel);
        String newCarouselStep = Wait(carousel).getAttribute("data-step");
        assert(!oldCarouselStep.equals(newCarouselStep));
        TakeScreenshot(driver, "tapCarousel");

        return this;
    }

    public BaseHomePage tapGetStarted()
    {
        Tap(getStarted);
        TakeScreenshot(driver,"tapGetStarted");

        return this;
    }
}
