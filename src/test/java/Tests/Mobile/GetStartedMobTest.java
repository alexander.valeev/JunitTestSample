package Tests.Mobile;

import Helpers.BaseTestFixture;
import Pages.Desktop.HomeDesPage;
import Pages.Desktop.SingUpDesPage;
import Pages.Mobile.HomeMobPage;
import Pages.Mobile.SingUpMobPage;
import org.junit.jupiter.api.Test;

public class GetStartedMobTest extends BaseTestFixture {
    @Test
    public void GetStartedMob()
    {
        setTestScreenPath(new Object(){}.getClass().getEnclosingMethod().getName());

        new HomeMobPage(driver)
                .tapCarousel()
                .tapCarousel()
                .tapCarousel()
                .tapCarousel()
                .tapCarousel()
                .tapGetStarted();
        new SingUpMobPage(driver)
                .enterEmail("some@email.com");
    }
}
