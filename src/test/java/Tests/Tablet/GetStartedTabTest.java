package Tests.Tablet;

import Helpers.BaseTestFixture;
import Pages.Desktop.HomeDesPage;
import Pages.Desktop.SingUpDesPage;
import Pages.Tablet.HomeTabPage;
import Pages.Tablet.SingUpTabPage;
import org.junit.jupiter.api.Test;

public class GetStartedTabTest extends BaseTestFixture {
    @Test
    public void GetStartedTab()
    {
        setTestScreenPath(new Object(){}.getClass().getEnclosingMethod().getName());

        new HomeTabPage(driver)
                .tapCarousel()
                .tapCarousel()
                .tapCarousel()
                .tapCarousel()
                .tapCarousel()
                .tapGetStarted();
        new SingUpTabPage(driver)
                .enterEmail("some@email.com");
    }
}
