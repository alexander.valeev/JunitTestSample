package Tests.Desktop;

import Helpers.BaseTestFixture;
import Pages.Desktop.HomeDesPage;
import Pages.Desktop.SingUpDesPage;
import org.junit.jupiter.api.Test;

public class GetStartedDesTest extends BaseTestFixture
{
    @Test
    public void GetStartedDes()
    {
        setTestScreenPath(new Object(){}.getClass().getEnclosingMethod().getName());

        new HomeDesPage(driver)
                .tapCarousel()
                .tapCarousel()
                .tapCarousel()
                .tapCarousel()
                .tapCarousel()
                .tapGetStarted();
        new SingUpDesPage(driver)
                .enterEmail("some@email.com");
    }
}
