package Helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage extends AppManager
{
    protected static WebDriver driver;
    protected static WebDriverWait wait;

    public BasePage(WebDriver driver)
    {
        BasePage.driver = driver;
        wait = new WebDriverWait(driver, 60);
    }

    protected static WebElement Wait(By element)
    {
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void Tap(By element)
    {
        Wait(element).click();
    }

    protected void Enter(By element, String text)
    {
        Wait(element).sendKeys(text);
    }
}
