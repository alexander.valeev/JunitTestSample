package Helpers;

import static Helpers.Enums.Browsers;
import static Helpers.Enums.Platforms;

public class BrowserModel {
    private Browsers browserKey;

    public Browsers getBrowserKey()
    {
        return browserKey;
    }

    private String browserString;

    public String getBrowserToString()
    {
        return browserString;
    }

    private Platforms platform;

    public Platforms getPlatform()
    {
        return platform;
    }

    private String version;

    public String getBrowserVersion()
    {
        return version;
    }

    public BrowserModel(Browsers browserKey, Platforms platform, String version)
    {
        this.browserKey = browserKey;

        switch (this.browserKey){
            case GoogleChrome:
                browserString = "chrome";
                break;
            case FireFox:
                browserString = "firefox";
                break;
            case Opera:
                browserString = "opera";
        }

        this.platform = platform;

        this.version = version;
    }
}
