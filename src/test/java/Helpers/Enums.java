package Helpers;

public class Enums {
    public enum Browsers
    {
        FireFox,
        GoogleChrome,
        Opera
    }

    public enum Platforms
    {
        Desktop,
        Tablet,
        Mobile
    }
}
