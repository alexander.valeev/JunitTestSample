package Helpers;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public abstract class AppManager
{
    private static BrowserModel browser;
    private static WebDriver driver;
    public final static String baseURL = "https://trello.com/";
    public static String testScreenPath;
    protected static int screenshotIndex;

    @Attachment
    public static byte[] TakeScreenshot(WebDriver driver, String screenshotName)
    {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        try
        {
            FileUtils.copyFile(scrFile, new File( testScreenPath + "/"
                    + screenshotIndex + "-" + screenshotName + ".png"), true);
            screenshotIndex++;
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("TakeScreenshot - " + screenshotName);
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    public static WebDriver GetWebDriver()
    {
        return driver;
    }

    public static void InitWebDriver(BrowserModel browserModel)
    {
        browser = browserModel;

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(browser.getBrowserToString());
        capabilities.setVersion(browser.getBrowserVersion());

        switch (browser.getBrowserKey())
        {
            case GoogleChrome:
                ChromeOptions options = new ChromeOptions();
                capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                break;
            case Opera:
                capabilities.setCapability("operaOptions", new HashMap<String, String>() {{
                    put("binary", "/usr/bin/opera");
                }});
                break;
        }

        // Selenoid capabilities
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableLog", true);
        capabilities.setCapability("enableVideo", false);

        try {
            driver = new RemoteWebDriver(new URL(
                    "http://localhost:4444/wd/hub"
            ), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        // ToDo move to selenoid capability
        switch (browser.getPlatform())
        {
            case Desktop:
                driver.manage().window().maximize();
                break;
            case Tablet:
                driver.manage().window().setSize(new Dimension(800, 800));
                break;
            case Mobile:
                driver.manage().window().setSize(new Dimension(450, 600));
                break;
        }

        System.out.println("InitWebDriver Done");
    }

    public void setTestConfig(String testName){
        setTestScreenPath(testName);
    }

    public void setTestScreenPath(String testName)
    {
        String projectPath = System.getProperty("user.dir");
        testScreenPath = projectPath + "/target/screenshot/"
                + browser.getPlatform() + "/"
                + browser.getBrowserToString() + "/"
                + testName;
    }

}
