package Helpers;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;


public class BaseTestFixture extends AppManager
{
    protected static WebDriver driver;

    @BeforeEach
    public void setUp()
    {
        screenshotIndex = 0;
        BrowserModel browserModel = new BrowserModel(Enums.Browsers.GoogleChrome, Enums.Platforms.Desktop, "77");
        InitWebDriver(browserModel);
        driver = GetWebDriver();

        driver.get(AppManager.baseURL);
    }

    @AfterEach
    public void cleanUp()
    {
        driver.quit();
    }
}
